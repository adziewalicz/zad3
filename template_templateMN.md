---
author: Adam Ziewalicz
title: Prezentacja 
subtitle: Yamaha Fazer
date: 
theme: Warsaw
output: beamer_presentation
header-includes: 
    \usepackage{xcolor}
    \usepackage{listings}
---

## Slajd 1

**Yamaha FZS 600 - ** \textcolor{red}{FAZER}
Yamaha FZS 600 Fazer pojawiła się w dealerstwach Yamahy w 1998 roku. Był to taki motocykl, na jaki wielu ówczesnych motocyklistów właśnie czekało. Fazer był niejako pomostem między przestarzałą już XJ 600, a dla wielu zbyt sportową YZF 600 Thundercat. Posiadając silnik tego drugiego, a nakedową pozycję tego pierwszego – Fazer okazał się marketingowym strzałem w dziesiątkę.

## Slajd 2

* Jednostka napędowa

  * Czterocylindrowa, rzędowa, czterosuwowa, chłodzona cieczą

  * Typ rozrządu DOHC

  * Pojemność – 599 cm3

  * Maksymalna moc – 95 KM przy 11500 obr./min

  * Maksymalny moment obrotowy – 61,2 Nm przy 9500 obr./min

  * Liczba zaworów – 4 na cylinder

  * Liczba gaźników – 4 Mikuni

  * Średnica gardzieli gaźników – 33 mm

<!--- UWAGA WAŻNE pusta linia odstępu -->



## Slajd 3 

### Historia

Producenci motocykli jak i samochodów, już w 90. latach doskonale znali przepis na stworzenie trwałego produktu w przystępnej cenie. Wystarczyło skorzystać z produkowanego już silnika, pozbawionego wad typowych dla świeżych konstrukcji i zespolić go z tanią w produkcji ramą i dostępnym osprzętem. Dodatkowo projektanci Fazera postanowili nadać mu nowoczesny design w miarę finansowych możliwości projektu. Dynamicznie poprowadzona linia 18-litrowego zbiornika paliwa płynnie łączy się z owiewką zaopatrzoną w dwie oddzielne lampy.

### 

Przeznaczeniem FZS 600 było głównie zatłoczone miasto, nie znaczy to jednak, że dłuższa eskapada jest niemożliwa. Właśnie temu kryterium podporządkowano ergonomiczną stronę motocykla. Jeździec zasiada wyprostowany na wygodnej, nawet dla dwojga kanapie, co w wielkomiejskich warunkach sprawdza się doskonale.

## Slajd 4 

\begin{alertblock}{Plusy}
Przy wyborze motocykla z rynku wtórnego każdy spogląda na trwałość oraz zapotrzebowanie na paliwo. Fazer w tej kwestii także prezentuje się z dobrej strony. Wedle relacji użytkowników walka o przetrwanie w zakorkowanym mieście pochłania do 6,5 l. Spokojnie pokonana zamiejska trasa zakończy się wynikiem nawet niższym niż 5 jednostek, natomiast utrzymywanie prędkości autostradowej okupione jest dobiciem do symbolicznych 5,5-6 litrów na setkę.
\end{alertblock}

## Slajd 5 
  
![](fazer.jpg){ height=50% width=50%}

 
